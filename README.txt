Plaats Reservation it reservation system for Plaats module.
Allowed reservation by Anonymous and registered users (depend of "permissions").

=Installation:
first need install all dependencies:
"plaats" - http://drupal.org/sandbox/fedik/1666468
"colorbox" - http://drupal.org/project/colorbox
"date" and "date_popup" - http://drupal.org/project/date

Then install as any other drupal module.

=Configuration:
First need edit Places and set "Allow reservation" if you want allow reservation
of this place.

Then in block Configuration (admin/structure/block) enable "Reservation Calendar".

Go to "permissions" admin/people/permissions#module-plaats_reservation
and set who can make reservation.

Also need set emails who get notification about new reservation, it can be done
in module configuration admin/config/content/plaats_reservation/email
and check other default option/messages there.

For reservation form can add some additional fields if need in
admin/config/content/plaats_reservation/fields


==How it works
Default when user navigate trough page with Plaats list he see no reservation link.
User select the date in "Reservation Calendar" for activate reservation system,
then system will check what Plaats allowed/available on this period
and will display reservation info for User + display message with selected reservation period.

After click "make reservation" user will get form (prefilled for registered users),
after sending this form user and admin will get email with reservation info, after that
admin should change status to allowed/rejected.

Default status is "Pending".

Default reservation period counts like:
"Date selected by User" + "Default period for the new reservation" that in configuration
