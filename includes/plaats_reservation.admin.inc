<?php
/**
 * @file
 * admin settings for plaats reservation
 */

/**
* plaats_reservation_admin_page
*/
function plaats_reservation_admin_page($form, &$form_state) {

  $plaats = entity_get_info('plaats');
  $plaats_views = array();
  foreach ($plaats['view modes'] as $name => $mode) {
    $plaats_views[$name] = $mode['label'];
  }

  $form['button_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Display reservation link'),
    '#description' => t('Configuration for the make reservation button'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    'plaats_reservation_plaats_views' => array(
      '#type' => 'checkboxes',
      '#title' => t('Show in view mode'),
      '#description' => t('Select place views types, where add the "Reserve This" button.'),
      '#default_value' => variable_get('plaats_reservation_plaats_views', array('full', 'tiny')),
      '#options' => $plaats_views,
    ),
    'plaats_reservation_plaats_title' => array(
      '#type' => 'textfield',
      '#title' => t('Link title in place view'),
      '#description' => t('Can be simple text or HTML code of a image.'),
      '#size' => 80,
      '#default_value' => variable_get('plaats_reservation_plaats_title', t('Make reservation')),
    ),
    //button weight
    'plaats_reservation_plaats_weight' => array(
      '#type' => 'weight',
      '#title' => t('Weight'),
      '#description' => t('Can be used for ordering'),
      '#default_value' => variable_get('plaats_reservation_plaats_weight', 8),
      '#delta' => 30,
    ),
  );

  $form['status_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Display reservation status'),
    '#description' => t('Configuration for display the reservation status'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    'plaats_reservation_plaats_status_views' => array(
      '#type' => 'checkboxes',
      '#title' => t('Show in view mode'),
      '#description' => t('Select place views types, where display the status.'),
      '#default_value' => variable_get('plaats_reservation_plaats_status_views', array('full', 'tiny', 'teaser')),
      '#options' => $plaats_views,
    ),
  //stattus weight
    'plaats_reservation_plaats_status_weight' => array(
      '#type' => 'weight',
      '#title' => t('Weight'),
      '#description' => t('Can be used for ordering'),
      '#default_value' => variable_get('plaats_reservation_plaats_status_weight', 6),
      '#delta' => 30,
    ),
  );

  //count of a available
  $form['status_count_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Display count of available'),
    '#description' => t('Show results counting the free children. Example: <em>Available 3 of 6</em>.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    'plaats_reservation_plaats_count_avail_views' => array(
      '#type' => 'checkboxes',
      '#title' => t('Show in view mode'),
      '#description' => t('Select place views types, where display count results.'),
      '#default_value' => variable_get('plaats_reservation_plaats_count_avail_views', array('teaser')),
      '#options' => $plaats_views,
    ),
    //stattus weight
    'plaats_reservation_plaats_count_avail_weight' => array(
      '#type' => 'weight',
      '#title' => t('Weight'),
      '#description' => t('Can be used for ordering'),
      '#default_value' => variable_get('plaats_reservation_plaats_count_avail_weight', 7),
      '#delta' => 30,
    ),
  );

  $form['popup_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Popup window for make rservation form'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    'plaats_reservation_usepopup' => array(
      '#type' => 'checkbox',
      '#title' => t('Use popup'),
      '#description' => t('Use popup window for the reservation form.'),
      '#default_value' => variable_get('plaats_reservation_usepopup', 1),
    ),
    'plaats_reservation_usepopup_width' => array(
      '#type' => 'textfield',
      '#title' => t('Popup width'),
      '#description' => t('Width for popup window.'),
      '#size' => 6,
      '#default_value' => variable_get('plaats_reservation_usepopup_width', 360),
    ),
    'plaats_reservation_usepopup_height' => array(
      '#type' => 'textfield',
      '#title' => t('Popup height'),
      '#description' => t('Height for popup window.'),
      '#size' => 6,
      '#default_value' => variable_get('plaats_reservation_usepopup_height', 480),
    ),
    'plaats_reservation_usepopup_close' => array(
      '#type' => 'textfield',
      '#title' => t('Close text'),
      '#description' => t('Text for the close button.'),
      '#size' => 12,
      '#default_value' => variable_get('plaats_reservation_usepopup_close', 'Close'),
    ),
  );

  //reservation period by calendar
  $form['def_period_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default period for the new reservation'),
    '#description' => t('Default period for the new reservation when make reservation via Reservation calendar, it need because in calendar can select only start date.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    'plaats_reservation_period_start_hour' => array(
      '#type' => 'textfield',
      '#title' => t('Start time'),
      '#description' => t('Default the start hour for reservation. Format: 10:00'),
      '#size' => 12,
      '#required' => TRUE,
      '#default_value' => variable_get('plaats_reservation_period_start_hour', '10:00'),
      '#element_validate' => array('_plaats_reservation_validate_hour'),
    ),
    'plaats_reservation_period_end_hour' => array(
      '#type' => 'textfield',
      '#title' => t('End time'),
      '#description' => t('Default the end hour for reservation.Format: 10:00'),
      '#size' => 12,
      '#required' => TRUE,
      '#default_value' => variable_get('plaats_reservation_period_end_hour', '20:00'),
      '#element_validate' => array('_plaats_reservation_validate_hour'),
    ),
    'plaats_reservation_period_days' => array(
      '#type' => 'textfield',
      '#title' => t('Days'),
      '#description' => t('Default count days.'),
      '#size' => 12,
      '#required' => TRUE,
      '#default_value' => variable_get('plaats_reservation_period_days', 1),
      '#element_validate' => array('_element_validate_number'),
    ),
  );

  $message = variable_get('plaats_reservation_success_message', array('value' => '<p>Thanks!</p>', 'format' => 'filtered_html'));
  $form['plaats_reservation_success_message'] = array(
    '#type' => 'text_format',
      '#title' => t('Thanks message.'),
    '#description' => t('Message after sending reservation.'),
    '#default_value' => $message['value'],
    '#format' => $message['format'],
  );
  $form['plaats_reservation_id_length'] = array(
    '#type' => 'textfield',
    '#title' => t('id length'),
    '#description' => t('Length for id that will be send for user. Example: if set 6 then will be send something like 000003, where 3 it reservation id.'),
    '#size' => 6,
    '#default_value' => variable_get('plaats_reservation_id_length', 6),
  );

  return system_settings_form($form);
}
/**
 * callback for email admin settings
 */
function plaats_reservation_admin_email($form, &$form_state) {

  $form['admin'] = array(
    '#type' => 'fieldset',
    '#title' => t('Admin email notify settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['admin']['plaats_reservation_email_admin_adress'] = array(
    '#type' => 'textfield',
    '#title' => t('Admin email'),
    '#description' => t('Where send notification about new reservation'),
    '#default_value' => variable_get('plaats_reservation_email_admin_adress', variable_get('site_mail', ini_get('sendmail_from'))),
  );

  $form['user'] = array(
    '#type' => 'fieldset',
    '#title' => t('User email notify settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['user']['plaats_reservation_email_subgect'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#description' => t('The subject line for email') . ' ' . t('Full line will be like:') . ' <em>"' . variable_get('plaats_reservation_email_subgect', t('Place reservation')) . ' - ' . t('Made a new reservation') . '"</em> ' . t('And') . ': <em>"' . variable_get('plaats_reservation_email_subgect', t('Place reservation')) . ' - ' . t('Reservation status update.') . '"</em>',
    '#default_value' => variable_get('plaats_reservation_email_subgect', t('Place reservation')),
  );
  $message = variable_get('plaats_reservation_email_make_order', array('value' => '<p>Thanks!</p><p>We tell you after checking!</p>', 'format' => 'filtered_html'));
  $form['user']['plaats_reservation_email_make_order'] = array(
    '#type' => 'text_format',
    '#title' => t('Notify about new reservation.'),
    '#default_value' => $message['value'],
    '#format' => $message['format'],
  );

  $message = variable_get('plaats_reservation_email_order_accept', array('value' => '<p>Hi!</p><p>You reservation was Accepted</p>', 'format' => 'filtered_html'));
  $form['user']['plaats_reservation_email_order_accept'] = array(
    '#type' => 'text_format',
    '#title' => t('Notify reservation "Accepted"'),
    '#default_value' => $message['value'],
    '#format' => $message['format'],
  );

  $message = variable_get('plaats_reservation_email_order_reject', array('value' => '<p>Hi!</p><p>You reservation was Rejected</p>', 'format' => 'filtered_html'));
  $form['user']['plaats_reservation_email_order_reject'] = array(
    '#type' => 'text_format',
    '#title' => t('Notify reservation "Rejected"'),
    '#default_value' => $message['value'],
    '#format' => $message['format'],
  );

  return system_settings_form($form);
}

function _plaats_reservation_validate_hour($element, &$form_state) {
  $v = explode(':', trim($element['#value']));
  if (!count($v) || !is_numeric($v[0]) || !is_numeric($v[1])
    || $v[0] > 24 || $v[1] > 60
  ) {
    form_error($element, t('%value is not valid time', array('%value' => $element['#value'])));
  }
}

