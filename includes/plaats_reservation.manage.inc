<?php
/**
 * @file
 */

/**
 * plaats_reservation_admin_page
 */
function plaats_reservation_manage_page($form, &$form_state, $plaats = NULL) {
  drupal_add_css(drupal_get_path('module', 'plaats_reservation') . '/css/plaats-reservation.admin.css');
  // Build the sortable table header.
  $header = array(
    'rid' => array('data' => t('id'), 'field' => 'r.rid', 'sort' => 'desc'),
    'plaats' => array('data' => t('Place')), // 'field' => 'r.pid'),
    'date_start' => array('data' => t('Start date'), 'field' => 'r.date_start'),
    'date_end' => array('data' => t('End date'), 'field' => 'r.date_end'),
    'status' => array('data' => t('Status'), 'field' => 'r.status'),
    'changed' => array('data' => t('Updated'), 'field' => 'r.changed' /*, 'sort' => 'desc'*/),
    'author' => array('data' => t('Author'), 'field' => 'r.uid'),
    'operations' => array('data' => t('Operations')),
  );

  $query = db_select('plaats_reservation', 'r')->extend('PagerDefault')->extend('TableSort');
  //$query->leftJoin('users', 'u', 'u.uid = r.uid');
  //$query->join('users', 'u', 'r.uid = u.uid');
  $query->fields('r', array('rid', 'uid'));
  //$query->fields('u', array('name'));
  $query->limit(30);
  $query->orderByHeader($header);
  if ($plaats) {
    $query->condition('r.pid', $plaats->pid);
  }

  $result = $query->execute()->fetchAllKeyed();

  $plaats_reservations = plaats_reservation_load_multiple(array_keys($result));

  // This is used to add usernames to the table below.
  $accounts = user_load_multiple(array_values($result));

  $options = array();
  $destination = drupal_get_destination();
  $status = array(
     'pending' => t('Pending'),
     'accept' => t('Accepted'),
     'reject' => t('Rejected')
  );

  //colect plaats ids
  $pids = array();
  foreach ($plaats_reservations as $reservation) {
    $pids[$reservation->pid] = $reservation->pid;
  }
  unset($reservation);

  //load plaatses
  $plaatses = (!$plaats) ? plaats_load_multiple($pids) : array($plaats->pid => $plaats);

  //build table content
  foreach ($plaats_reservations as $k => $reservation) {
    $plaats = $plaatses[$reservation->pid];
    $options[$reservation->rid] = array(
       'rid' => $reservation->rid,
       'plaats' => l($plaats->name, 'admin/content/plaats_reservation/' . $reservation->pid . '/filter', array('attributes' => array('title' => t('Filter all reservation for @plaats', array('@plaats' => $plaats->name))))),
       'date_start' => format_date($reservation->date_start, 'short'),
       'date_end' => format_date($reservation->date_end, 'short'),
       'status' => '<span class="' . $reservation->status . '">' . $status[$reservation->status] . '</span>',
       'changed' => format_date($reservation->changed, 'short'),
       'author' => theme('username', array('account' => $accounts[$reservation->uid])),
       'operations' => '',
    );
//     if ($reservation->status == 'pending') {
//       $status_form = array(
//         '#type' => 'select',
//         '#title' => t('Reservation status'),
//         '#title_display' => 'invisible',
//         '#default_value' => $reservation->status,
//         '#options' => array(
//           'pending' => t('Pending'),
//           'accept' => t('Accepted'),
//           'reject' => t('Rejected')
//         ),
//       );

//       $options[$reservation->rid]['status']= drupal_render($status_form);
//       $action = TRUE;
//     }
    // add operations
    $operations = array();
    $operations['edit'] = array('title' => t('edit'));
//    if ($reservation->status == 'pending') {
      $operations['edit']['href'] = 'admin/content/plaats_reservation/' . $reservation->rid . '/edit';
      $operations['edit']['query'] = $destination;
//    }

//     $operations['view'] = array(
//        'title' => t('view'),
//        'href' => 'plaats_reservation/check/' . $reservation->rid ,//.$reservation->created,
//     );

    $operations['delete'] = array(
      'title' => t('delete'),
      'href' => 'admin/content/plaats_reservation/' . $reservation->rid . '/delete',
      'query' => drupal_get_destination(),
    );

    $options[$reservation->rid]['operations'] = array(
       'data' => array(
        '#theme' => 'links__node_operations',
        '#links' => $operations,
        '#attributes' => array('class' => array('links', 'inline')),
      ),
    );
  }

  $form['plaats_reservation'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No reservation available'),
  );
  $form['pager'] = array('#markup' => theme('pager'));

  return $form;
}

/**
* plaats_reservation_reservation form for users
*/
function plaats_reservation_reservation_form($form, &$form_state, $plaats = NULL) {
//  global $user;

  if (!empty($_SESSION['plaats_reservation']['success'])
    && $_SESSION['plaats_reservation']['success']
  ) {
    //show message
    $message = variable_get('plaats_reservation_success_message', array('value' => '<p>Thanks!</p>', 'format' => 'filtered_html'));
    $form['success'] = array('#markup' => '<div class="success-message">' . $message['value'] . '</div>');
    //show information about reservation
    $id = plaats_reservation_get_id($_SESSION['plaats_reservation']['rid']);
    $info_message = '<br />' . t('You reservation saved with ID <b>%id</b>', array('%id' => $id)) ;
    //$info_message .= '<br />' . t('Status you can check by using <b>!link</b>', array('!link' => l(t('this link'), 'plaats_reservation/check/' . $id, array('attributes' => array('target' => '_blank'))))) . '<br />';
    $form['info_message'] = array('#markup' => $info_message);
    //show Back link
    if (!variable_get('plaats_reservation_usepopup', 1)) {
      $form['actions'] = array('#type' => 'actions');
      $form['actions']['back'] =array(
        '#markup' => l(t('Back'), 'plaats/' . $plaats->machine_name),
      );
    }
    //clean session
    unset($_SESSION['plaats_reservation']);

    return $form;
  }
  if (empty($_SESSION['plaats_reservation'])
    || empty($_SESSION['plaats_reservation']['date_start'])
    || empty($_SESSION['plaats_reservation']['date_end'])
    || empty($plaats)
  ) {
    drupal_set_message(t('Please first select the start date!'), 'warning');
    return $form;
  }

  $form = plaats_reservation_manage_reservation_form($form, $form_state);//drupal_get_form('plaats_reservation_manage_reservation_form');

  $form['pid'] = array(
    '#type' => 'hidden',
    '#value' => $plaats->pid,
  );

  $form['date_start'] = array(
      '#type' => 'hidden',
      '#value' => $_SESSION['plaats_reservation']['date_start'],
  );
  $form['date_end'] = array(
      '#type' => 'hidden',
      '#value' => $_SESSION['plaats_reservation']['date_end'],
  );
  $form['status'] = array(
      '#type' => 'hidden',
      '#value' => 'pending',
  );
  $form['actions']['submit']['#value'] = t('Send');

  return $form;
}
/**
 * submit fuction for plaats_reservation_reservation_form user form
 */
function plaats_reservation_reservation_form_submit($form, &$form_state) {

  $plaats_reservation = $form_state['plaats_reservation'];

  // Attach submit handlers from Field module.
  entity_form_submit_build_entity('plaats_reservation', $plaats_reservation, $form, $form_state);
  field_attach_submit('plaats_reservation', $plaats_reservation, $form, $form_state);
  plaats_reservation_save($plaats_reservation);

  switch ($plaats_reservation->save_status) {
    case SAVED_NEW:
      //notify user
      plaats_reservation_user_notify($plaats_reservation, 'reservation_new');
      //mark session as done
      drupal_session_start();
      $_SESSION['plaats_reservation']['success'] = TRUE;
      $_SESSION['plaats_reservation']['rid'] = $plaats_reservation->rid;
      drupal_set_message(t('Reservation sended.'));
      watchdog('plaats_reservation', 'Created new reservation.', array(), WATCHDOG_NOTICE, l(t('edit'), 'admin/content/plaats_reservation/' . $plaats_reservation->rid . '/edit'));
      break;
    case (FALSE):
      drupal_set_message(t('Error sending reservation.'), 'error');
      break;
  }
}

/**
 * plaats_reservation_manage_reservation_form
 */
function plaats_reservation_manage_reservation_form($form, &$form_state, $plaats_reservation = NULL) {
  global $user;

  if (!$plaats_reservation) {
    $plaats_reservation = (object) array(
      'pid' => '',
      'uid' => $user->uid,
      'created' => time(),
      'changed' => time(),
      'date_start' => '',
      'date_end' => '',
      'status' => 'pending' // (pending/accept/reject)
    );
  }
  $form_state['plaats_reservation'] = $plaats_reservation;

  //date created
  $form['created'] = array('#type' => 'hidden', '#value' => $plaats_reservation->created);
  //date changed
  $form['changed'] = array('#type' => 'hidden', '#value' => time());
  //user id
  $form['uid'] = array( '#type' => 'hidden', '#value' => $plaats_reservation->uid);

  //plaatss list
  //get the plaatss list for a select field
  $plaatss_list = array('' => '== ' . t('Select place') . ' ==');
  $plaatss_list += plaats_get_plaats_select_list();
  $form['pid'] = array(
    '#type' => 'select',
    '#title' => t('Select place'),
    '#required' => TRUE,
    '#description' => t('Select the place for reservation'),
    '#default_value' => $plaats_reservation->pid,
    '#weight' => 1,
    '#options' => $plaatss_list,
  );
  //date start
  $form['date_start']=array(
    '#type' => 'date_popup',
    '#title' => t('Start date'),
    '#description' => t('Reservation start date'),
    '#default_value' => (!empty($plaats_reservation->date_start)) ? format_date($plaats_reservation->date_start, 'custom', 'Y-m-d H:i:s'):'',//date('Y-m-d H:i:s', $plaats_reservation->date_start),
    '#date_format' => 'Y-m-d H:i:s',
    '#date_year_range' => '0:+3',
    '#required' => TRUE,
    '#weight' => 1.3,
  );
  //date end
  $form['date_end']=array(
    '#type' => 'date_popup',
    '#title' => t('End date'),
    '#description' => t('Reservation end date'),
    '#default_value' => (!empty($plaats_reservation->date_end)) ? format_date($plaats_reservation->date_end, 'custom', 'Y-m-d H:i:s'):'',//$plaats_reservation->date_end,
    '#date_format' => 'Y-m-d H:i:s',
    '#date_year_range' => '0:+3',
    '#required' => TRUE,
    '#weight' => 1.4,
  );
  //status
  $form['status']=array(
    '#type' => 'select',
    '#title' => t('Reservation status'),
    '#description' => t('Select the reservation status'),
    '#default_value' => $plaats_reservation->status,
    '#weight' => 11,
    '#options' => array(
      'pending' => t('Pending'),
      'accept' => t('Accepted'),
      'reject' => t('Rejected')
    ),
  );

  $form['#validate'][] = 'plaats_reservation_manage_reservation_validate';
  // action buttons
  $form['actions'] = array('#type' => 'actions');
  if ($plaats_reservation->status == 'pending' ) {
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save')
    );
  }
  if (!empty($plaats_reservation->rid)) {
    $form['actions']['delete'] = array(
        '#type' => 'submit',
        '#value' => t('Delete'),
        '#submit' => array('plaats_reservation_form_delete_submit'),
    );
  }

  //name//email - by extra fields
  // Attach fields from Field module.
  field_attach_form('plaats_reservation', $plaats_reservation, $form, $form_state);

  //fill info about user if new item
  if (!$plaats_reservation->pid && $user->uid) {
    if (!empty($form['plaats_reservation_name'])) {
      $lang = $form['plaats_reservation_name']['#language'];
      $form['plaats_reservation_name'][$lang][0]['value']['#default_value'] = $user->name;
    }
    if (!empty($form['plaats_reservation_email'])) {
      $lang = $form['plaats_reservation_email']['#language'];
      $form['plaats_reservation_email'][$lang][0]['value']['#default_value'] = $user->mail;
    }

  }

  return $form;
}
/**
 * plaats_reservation_manage_reservation_validate
 * check date start and date end
 */
function plaats_reservation_manage_reservation_validate($form, &$form_state) {

  if ($form['#form_id'] == 'plaats_reservation_manage_reservation_form') {
    //convert to unix time
    $form_state['values']['date_start'] = (!is_array($form_state['values']['date_start'])) ? strtotime($form_state['values']['date_start']) : '';
    $form_state['values']['date_end'] = (!is_array($form_state['values']['date_end'])) ? strtotime($form_state['values']['date_end']) : '';
  }
  //check whether is allowed reservation
  if (!empty($form_state['values']['pid'])) {
    $plaats = plaats_load($form_state['values']['pid']);
    if (!field_get_items('plaats', $plaats, 'plaats_reservation_is_allow')) {
      form_error($form['pid'], t('For <em>%plaats</em> reservation not allowed.' , array('%plaats' => $plaats->name)));
    }
  }
  //check date start and date end
  if ($form_state['values']['date_end'] < $form_state['values']['date_start']) {
    form_error($form['date_end'], $message = t('"End date" cannot be less as "Start date".'));
  }
  //validate email
  if (!empty($form_state['values']['plaats_reservation_email'])) {
    $email = $form_state['values']['plaats_reservation_email'];
    $email = array_shift($email);
    if (!valid_email_address(trim($email[0]['value']))) {
      form_error($form['plaats_reservation_email'], t('"%mail" is not a valid email address', array('%mail' => $email[0]['value'])));
    }
  }

  //check if this plase  already reserved on this date
  $rid = (!empty($form_state['plaats_reservation']->rid))?$form_state['plaats_reservation']->rid : NULL;
  $reservations = plaats_reservation_check_dates($form_state['values']['date_start'], $form_state['values']['date_end'], $form_state['values']['pid'], $rid);

  if (!empty($reservations)) {
    //message for manager
    if ($form['#form_id'] == 'plaats_reservation_manage_reservation_form') {
      $status = array(
         'pending' => t('Pending'),
         'accept' => t('Accepted'),
         'reject' => t('Rejected')
      );
      $msg = t('This place already reserved for a similar period.');
      if (!empty($reservations['date_start'])) {
        $msg .= '<br />' . t('Have a similar start date') . ': <ul>';
        foreach ($reservations['date_start'] as $reservation) {
          $msg .='<li><a target="_blank" title="' . t('Opens in new window') . '" href="' . url('admin/content/plaats_reservation/' . $reservation->rid . '/edit') . '"><b>' . t('Status') . ':</b> ' . $status[$reservation->status] . ', <b>' . t('Start date') . ':</b> ' . format_date($reservation->date_start, 'short') . ', <b>' . t('End date') . ':</b> ' . format_date($reservation->date_end, 'short') . '</a></li>';
        }
        $msg .= '</ul>';
      }
      if (!empty($reservations['date_end'])) {
        $msg .= '<br />' . t('Have a similar end date') . ': <ul>';
        foreach ($reservations['date_end'] as $reservation) {
          $msg .='<li><a target="_blank" title="' . t('Opens in new window') . '" href="' . url('admin/content/plaats_reservation/' . $reservation->rid . '/edit') . '"><b>' . t('Status') . ':</b> ' . $status[$reservation->status] . ', <b>' . t('Start date') . ':</b> ' . format_date($reservation->date_start, 'short') . ', <b>' . t('End date') . ':</b> ' . format_date($reservation->date_end, 'short') . '</a></li>';
        }
        $msg .= '</ul>';
      }

      form_error($form['pid'], filter_xss($msg));
    }
    else {
    //message for users
      form_error($form['pid'], t('This place already reserved for a similar period.'));
    }
  }
}
/**
* plaats_reservation_manage_reservation_form
*/
function plaats_reservation_manage_reservation_form_submit($form, &$form_state) {
  $plaats_reservation = $form_state['plaats_reservation'];

  // Attach submit handlers from Field module.
  entity_form_submit_build_entity('plaats_reservation', $plaats_reservation, $form, $form_state);
  field_attach_submit('plaats_reservation', $plaats_reservation, $form, $form_state);
  plaats_reservation_save($plaats_reservation);

  //krumo($form,$form_state);exit;
  switch ($plaats_reservation->save_status) {
    case SAVED_NEW:
      //notify user
      plaats_reservation_user_notify($plaats_reservation, 'reservation_new');
      drupal_set_message(t('New reservation created.'));
      watchdog('plaats_reservation', 'Created new reservation.', array(), WATCHDOG_NOTICE, l(t('edit'), 'admin/content/plaats_reservation/' . $plaats_reservation->rid . '/edit'));
      break;
    case SAVED_UPDATED:
      //notify user if status changed
      if ($plaats_reservation->original->status != $plaats_reservation->status) {
        plaats_reservation_user_notify($plaats_reservation, 'reservation_upd');
      }
      drupal_set_message(t('Reservation updated.'));
      watchdog('plaats_reservation', 'Updated reservation.', array(), WATCHDOG_NOTICE, l(t('edit'), 'admin/content/plaats_reservation/' . $plaats_reservation->rid . '/edit'));
      // Clear the page and block caches to avoid stale data.
      cache_clear_all();
      break;
    case (FALSE):
      drupal_set_message(t('Error saving reservation.'), 'error');
      break;
  }
  $form_state['redirect'] = 'admin/content/plaats_reservation';
}

/**
* delete confirmation
*/
function plaats_reservation_form_delete_confirm($form, &$form_state, $plaats_reservation) {

  $form['#plaats_reservation'] = $plaats_reservation;
  //check if  deletion allowed
  if ($plaats_reservation->status != 'reject' || $plaats_reservation->date_end < time() ) {
    $form['deletion_notallowed'] = array(
      '#markup' => '<b>' . t('Deletion not allowed') . '</b><br />' . t('You can delete only outdated reservations or reservations with status "Rejected"'),
    );
    $form['actions'] = array('#type' => 'actions');
    $form['actions']['cancel'] = array(
        '#type' => 'link',
        '#title' => t('Cancel'),
        '#href' => 'admin/content/plaats_reservation',
    );
    return $form;
  }

  // Always provide entity id in the same form key as in the entity edit form.
  $form['rid'] = array('#type' => 'value', '#value' => $plaats_reservation->rid);
  $form['type'] = array('#type' => 'value', '#value' => 'plaats_reservation');

  $plaats = plaats_load($plaats_reservation->pid);
  //date format
  $date_start = format_date($plaats_reservation->date_start, 'short');
  $date_end = format_date($plaats_reservation->date_end, 'short');

  return  confirm_form($form,
  t('Are you sure you want to delete?'),
  'admin/content/plaats_reservation',
  t('Are you sure you want to delete the reservation on a dates <b>%date_start - %date_end</b> for a place <b>%title</b> ?',
      array(
        '%title' => $plaats->name,
        '%date_start' => $date_start,
        '%date_end' => $date_end
      )
   ) . '<br />' . t('This action cannot be undone.'),
  t('Delete'),
  t('Cancel'));
}
/**
* plaats_reservation_form_delete_submit
*/
function plaats_reservation_form_delete_submit($form, &$form_state) {
  $plaats_reservation = $form_state['plaats_reservation'];
  $destination = array();
  if (isset($_GET['destination'])) {
    $destination = drupal_get_destination();
    unset($_GET['destination']);
  }
  $form_state['redirect'] = array('admin/content/plaats_reservation/' . $plaats_reservation->rid . '/delete', array('query' => $destination));
}

/**
* Execute plaats deletion
*/
function plaats_reservation_form_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $plaats_reservation = $form['#plaats_reservation'];
    $plaats = plaats_load($plaats_reservation->pid);
    //date format
    $date_start = format_date($plaats_reservation->date_start, 'short');
    $date_end = format_date($plaats_reservation->date_end, 'short');

    plaats_reservation_delete($form_state['values']['rid']);
    watchdog('plaats_reservation', 'Reservation for place %name deleted.', array('%name' => $plaats->name));
    drupal_set_message(t('Reservation on a dates %date_start - %date_end for a place %name has been deleted.', array('%name' => $plaats->name, '%date_start' => $date_start, '%date_end' => $date_end)));
    $form_state['redirect'] = array('admin/content/plaats_reservation');
  }

}
