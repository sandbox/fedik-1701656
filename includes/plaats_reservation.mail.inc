<?php
/**
 * @file send emails for costumers
 */

/**
 * sen notification for user about reservation
 * @param $reservation a plaats reservation object
 * @param string $key
 *   one of:
 *     reservation_new
 *     reservation_upd
 *
 */

function plaats_reservation_user_notify($reservation, $key) {
  global $language;

  $email = array_values($reservation->plaats_reservation_email);
  $user_name = array_values($reservation->plaats_reservation_name);
  $to = $user_name[0][0]['value'] . ' <' . $email[0][0]['value'] . '>';
  $params = array(
      'reservation' => $reservation,
  );
  if ($key == 'reservation_new') {//if new then send email also for admin
    $to_admin = variable_get('plaats_reservation_email_admin_adress', variable_get('site_mail', ini_get('sendmail_from')));
    drupal_mail('plaats_reservation', 'reservation_admin', $to_admin, $language->language, $params);
  }
//   $file = file_load(258);
//   $file->filepath = drupal_realpath($file->uri);
//   $params['attachments'] = array($file);
  drupal_mail('plaats_reservation', $key, $to, $language->language, $params);
}

/**
 * hook_mail($key, &$message, $params)
 * Prepare a message based on parameters; called from drupal_mail().
 * @param string $key
 *   Must be one of:
 *     reservation_new
 *     reservation_upd
 *     reservation_admin
 * @param array $message
 *   The message array, containing at least the following keys:
 *   - id: An ID to identify the mail sent.
 *   - to: The address or addresses the message will be sent to. The formatting of this string must comply with RFC 2822.
 *   - subject: Subject of the e-mail to be sent.
 *   - from: The address the message will be marked as being from
 *   - headers: An array containing at least a 'From' key.
 *   - language: The preferred message language.
 * @param array $params
 *   An array of parameters supplied by the caller of drupal_mail().
 *
 */
function plaats_reservation_mail($key, &$message, $params) {
  if ($message['module'] == 'plaats_reservation') {

    switch ($key) {
      case 'reservation_new':
        _plaats_reservation_mail_body_new($message, $params);
        break;
      case 'reservation_upd':
        _plaats_reservation_mail_body_upd($message, $params);
        break;
      case 'reservation_admin':
        _plaats_reservation_mail_body_admin($message, $params);
        break;

    }
  }
}

/**
 * build email body for notify about new reservation
 */
function _plaats_reservation_mail_body_new(&$message, $params) {
  $id = plaats_reservation_get_id($params['reservation']->rid);

  $msg = variable_get('plaats_reservation_email_make_order', array('value' => '<p>Thanks!</p><p>We tell you after checking!</p>'));
  $build = plaats_reservation_view($params['reservation'], 'email', $message['language']);

  $message['subject'] = variable_get('plaats_reservation_email_subgect', t('Place reservation')) . ' - ' . t('Made a new reservation');
  $message['body'][] = $msg['value'];
  //$message['body'][] = '<br />'.t('Status you can check by using <b>!link</b>',array('!link'=>l(t('this link'),'plaats_reservation/check/'.$id,array('attributes'=>array('target'=>'_blank'))))).'<br />';

  //add reservation info
  $message['body'][] = theme('plaats_reservation', array('elements' => $build));//drupal_render($build);
}

/**
* build email body for notify about update reservation status
*/
function _plaats_reservation_mail_body_upd(&$message, $params) {
  $id = plaats_reservation_get_id($params['reservation']->rid);

  $build = plaats_reservation_view($params['reservation'], 'email', $message['language']);

  $message['subject'] = variable_get('plaats_reservation_email_subgect', t('Place reservation')) . ' - ' . t('Reservation status update');

  if ($params['reservation']->status == 'accept') {
    $message['subject'] .= ' ' . t('Accepted');

    $msg = variable_get('plaats_reservation_email_order_accept', array('value' => '<p>Hi!</p><p>You reservation was Accepted</p>'));
    $message['body'][] = $msg['value'];
    $message['body'][] = t('You reservation ID <b>%id</b>', array('%id' => $id));
  }

  if ($params['reservation']->status == 'reject') {
    $message['subject'] .= ' ' . t('Rejected');

    $msg = variable_get('plaats_reservation_email_order_reject', array('value' => '<p>Hi!</p><p>You reservation was Rejected</p>'));
    $message['body'][] = $msg['value'];
  }

  //add reservation info
  $message['body'][] = theme('plaats_reservation', array('elements' => $build));//drupal_render($build);
}

/**
* build email body for admin about new reservation
*/
function _plaats_reservation_mail_body_admin(&$message, $params) {
  $build = plaats_reservation_view($params['reservation'], 'email', $message['language']);

  $message['subject'] = variable_get('plaats_reservation_email_subgect', t('Place reservation')) . ' - ' . t('Made a new reservation');

  $message['body'][] = t('Someone made a new reservation on your site. Check it!');
  $message['body'][] = t('Created') . ': ' . format_date($params['reservation']->created);
  $message['body'][] = t('IP address') . ': ' . ip_address();
  $message['body'][] = '<b>' . t('Edit') . ':</b> ' . l(t('click here to edit'), 'admin/content/plaats_reservation/' . $params['reservation']->rid . '/edit', array('attributes' => array('target' => '_blank')));
  //$message['body'][] = '<b>'.t('View').':</b> '.l(t('click here to view'),'plaats_reservation/check/'.$params['reservation']->rid,array('attributes'=>array('target'=>'_blank')));

  //add reservation info
  $message['body'][] = theme('plaats_reservation', array('elements' => $build));//drupal_render($build);
}

