<?php
/**
 * krumo($variables)
 * $plaats_reservation
 * $plaats
 * $date_start
 * $date_end
 * $view_mode (full/email)
 */
?>
<?php if ($view_mode != 'email'): ?>
<div id = "plaats-reservation-<?php print $plaats_reservation->rid; ?>" class="<?php print $classes; ?> <?php print $zebra; ?> clearfix"<?php print $attributes; ?>>
	<div class="operations"><?php  print render($content['operations']); ?></div>
	<h2 <?php print $title_attributes; ?>><?php print $plaats->name; ?></h2>
	<div class="content"<?php print $content_attributes; ?>>
    	<?php  print render($content); ?>
    	<div class="reservation-dates">
    	<b><?php  print t('Start date'); ?>:</b>
    	<span class="start-date"><?php  print $date_start; ?></span>
    	<br />
    	<b><?php  print t('End date'); ?>:</b>
    	<span class="end-date"><?php  print $date_end; ?></span>
    	</div>
    	<div class="reservation-status">
    	<b><?php  print t('Status'); ?>:</b>
    	<span class="<?php  print $plaats_reservation->status; ?>"><?php  print $status; ?></span>
    	</div>
  	</div>
</div>
<?php else: //for email message ?>
<div id = "plaats-reservation-<?php print $plaats_reservation->rid; ?>">
<fieldset>
<legend <?php print $title_attributes; ?>><b><?php print $plaats->name; ?></b></legend>
<div class="reservation-status <?php  print $plaats_reservation->status; ?>">
<?php  print t('Status'); ?>: <b><?php  print $status; ?></b>
</div>
<?php  print render($content); ?>
<div class="reservation-dates">
 	<div><?php  print t('Start date'); ?>: <?php  print $date_start; ?></div>
	<div><?php  print t('End date'); ?>: <?php  print $date_end; ?></div>
</div>
</fieldset>
</div>
<?php endif; ?>