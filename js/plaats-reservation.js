(function($) {

  Drupal.behaviors.plaatsReservation = {
    attach: function(context, settings) {
    	if (!$.isFunction($.colorbox) || !settings.plaatsReservation) {return;}
    	settings.plaatsReservation.onCleanup = plaatsReservationRefresh;
    	$('div.plaats a.plaats-reservation-link',context).colorbox(settings.plaatsReservation);
    },
    detach: function (context, settings, trigger){
    	if (!$.isFunction($.colorbox)) {return;}
  		$('div.plaats a.plaats-reservation-link', context).colorbox.remove();
  	}
  };
  //refresh page after close popup
  plaatsReservationRefresh = function(){
	  location = location;
  }

})(jQuery);